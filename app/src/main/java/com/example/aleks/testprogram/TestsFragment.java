package com.example.aleks.testprogram;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TestsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TestsFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "history_list";

    private ListView listView;
    private Button button;
    private ListView listView2;
    private Button button2;
    private Question1Adapter adapter;
    private Question2Adapter adapter2;
    private List<QuestionTypeOne> list;
    private List<QuestionTypeTwo> listTwo;
    private int firstLevel = 0;
    private int secondLevel = 0;
    private Button startButton;
    private long startTesting;

    public TestsFragment() {
        // Required empty public constructor
    }

    public static TestsFragment newInstance() {
        TestsFragment fragment = new TestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tests, container, false);
        startButton = (Button) view.findViewById(R.id.startButton);
        startButton.setOnClickListener(this);
        listView = (ListView) view.findViewById(R.id.testListView1);
        listView2 = (ListView) view.findViewById(R.id.testListView2);
        list = QuestionTypeOneList.getInstance().getList();
        listTwo = QuestionTypeTwoList.getInstance().getList();
        View v = getActivity().getLayoutInflater().inflate(R.layout.list_footer_question1, null);
        View v2 = getActivity().getLayoutInflater().inflate(R.layout.list_footer_question1, null);
        button = (Button) v.findViewById(R.id.testButton);
        button2 = (Button) v2.findViewById(R.id.testButton);
        button2.setText("Завершить");
        adapter = new Question1Adapter(list);
        adapter2 = new Question2Adapter(listTwo);
        listView.addFooterView(v);
        listView.setAdapter(adapter);
        listView2.addFooterView(v2);
        listView2.setAdapter(adapter2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstLevel = checkAnswer1();
                if (firstLevel == -1) {
                    Toast.makeText(getContext(), "Ответьте на все вопросы", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("mytag", "correct " + firstLevel);
                    listView.setVisibility(View.GONE);
                    listView2.setVisibility(View.VISIBLE);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secondLevel = checkAnswer2();
                if (secondLevel == -1) {
                    Toast.makeText(getContext(), "Ответьте на все вопросы", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("mytag", "correct " + secondLevel);
                    Toast.makeText(getContext(), "первый уровень правильно:" + firstLevel + "второй уровень правильно:" + secondLevel, Toast.LENGTH_SHORT).show();
                    clearValues();
                    finishTesting();
                }
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startButton: {
                startTesting();
                break;
            }
        }
    }

    private void clearValues() {
        list = QuestionTypeOneList.getInstance().getList();
        listTwo = QuestionTypeTwoList.getInstance().getList();
    }

    private void startTesting() {
        startButton.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        startTesting = System.currentTimeMillis();
    }

    private void finishTesting() {
        listView2.setVisibility(View.GONE);
        startButton.setVisibility(View.VISIBLE);
        HistoryItem item = new HistoryItem(startTesting, System.currentTimeMillis() - startTesting, firstLevel, secondLevel);
        File file = new File(getContext().getFilesDir(), "history");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            Log.d("mytag", file.getAbsolutePath());
            FileInputStream fin = null;
            ObjectInputStream ois = null;
            fin = new FileInputStream(file);
            List<HistoryItem> list;
            try {
                ois = new ObjectInputStream(fin);
                list = (List<HistoryItem>) ois.readObject();
                ois.close();
            } catch (EOFException ex) {
                list = new ArrayList<>();
            }
            fin.close();
            Log.d("mytag", list.size() + "");
            list.add(item);
            file.delete();
            file.createNewFile();
            FileOutputStream fout = null;
            ObjectOutputStream oos = null;
            fout = new FileOutputStream(file);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(list);
            oos.close();
            fout.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private int checkAnswer1() {
        int i = 0;
        for (QuestionTypeOne question : list) {
            if (question.getUserAnswer().equals("")) {
                return -1;
            } else {
                if (question.getUserAnswer().equals(question.getAnswer()))
                    i++;
            }

        }
        return i;
    }

    private int checkAnswer2() {
        int i = 0;
        for (QuestionTypeTwo question : listTwo) {
            if (question.getUserAnswer().size() == 0) {
                return -1;
            } else {
                if (compareLists(question.getUserAnswer(), question.getAnswer()))
                    i++;
            }

        }
        return i;
    }

    private boolean compareLists(List<String> list1, List<String> list2) {
        return list1.size() == list2.size() && list1.containsAll(list2);
    }

    private class Question1Adapter extends ArrayAdapter<QuestionTypeOne> {

        Question1Adapter(@NonNull List<QuestionTypeOne> objects) {
            super(TestsFragment.this.getContext(), R.layout.layout_question1_item, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            final QuestionTypeOne question = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.layout_question1_item, null);
            }
            TextView textView = (TextView) convertView.findViewById(R.id.question1ItemTextView);
            textView.setText(question.getQuestion());
            RadioGroup radioGroup = (RadioGroup) convertView.findViewById(R.id.question1RadioGroup);
            radioGroup.removeAllViews();
            for (int i = 0; i < question.getVariants().size(); i++) {
                RadioButton radioButton = new RadioButton(TestsFragment.this.getContext());
                String str = question.getVariants().get(i);
                radioButton.setText(question.getVariants().get(i));
                if (str.equals(question.getUserAnswer()))
                    radioButton.setChecked(true);
                radioGroup.addView(radioButton);
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RadioButton button = (RadioButton) v;
                        question.setUserAnswer(button.getText().toString());
                    }
                });
            }
            return convertView;
        }
    }

    private class Question2Adapter extends ArrayAdapter<QuestionTypeTwo> {

        Question2Adapter(@NonNull List<QuestionTypeTwo> objects) {
            super(TestsFragment.this.getContext(), R.layout.layout_question2_item, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            final QuestionTypeTwo question = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.layout_question2_item, null);
            }
            TextView textView = (TextView) convertView.findViewById(R.id.question2ItemTextView);
            textView.setText(question.getQuestion());
            LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.question2LinearLayout);
            layout.removeAllViews();
            for (String str : question.getVariants()) {
                final CheckBox checkBox = new CheckBox(TestsFragment.this.getContext());
                checkBox.setText(str);
                if (question.getUserAnswer().contains(str))
                    checkBox.setChecked(true);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            question.getUserAnswer().add(checkBox.getText().toString());
                        } else {
                            question.getUserAnswer().remove(checkBox.getText().toString());
                        }
                    }
                });
                layout.addView(checkBox);
            }
            return convertView;
        }
    }

}
