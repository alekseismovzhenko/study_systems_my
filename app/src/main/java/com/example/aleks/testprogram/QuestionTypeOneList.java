package com.example.aleks.testprogram;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleks on 30.03.2017.
 */

public class QuestionTypeOneList {
    private static QuestionTypeOneList instance;
    private static String[] questions = new String[]{
            "Совокупность набора характеристик обучаемого, измеряемых во время работы системы с обучаемым и определяющих степень усвоения им знаний по изучаемому предмету и методов (правил) обработки этого набора.",
            "Сколько характеристик составляет модель обучения:",
            "Для построения траектории обучения, нужно иметь такую информацию:",
            "Сколько элементов в математической модели обучаемого:",
            "Что подразумевают под мотивацией в математической модели обучаемого?",
            "Какой компонент, не входит в набор компонентов модели обучения через тестирование:",
            "Выберите из какий компонентов складывается интегральная оценка:",
            "Отношение детализации знаний b — это … ",
            "Что называют под программное средство (программный комплекс) или программно-технический комплекс, предназначенный для решения определенных педагогических задач, имеющий предметное содержание и ориентированный на взаимодействие с обучаемым.",
            "Укажите, что не относится к отрицательным стороне компьютерного обучения:",
            "По каким факторам нет группировки целей применения компьютерного обучения:"
    };
    private static String[][] answers = new String[][]{
            {"Модель обучаемого", "Модель управления обучаемого", "модель обучения как управление", "Адаптивная модель обучаемого"},
            {"1", "2", "3", "4"},
            {"об обучении", "о модели обучения", "о модели обучаемого", "о порядоке подачи информаци"},
            {"1", "2", "3", "4", "5", "6"},
            {"Определение психологического настроя обучаемого и поддержание положительного отношения к обучению.", "Наличие конечной цели во время обучения.", "Определить настрой обучаемого", "определить отношение к обучению обучаемого"},
            {"Определение уровня знаний учебных элементов", "Тестирование", "Оценка способности к обучению", "Определение базовой оценки", "Оценка скорости забывания"},
            {"Интегральаная и линейная", "Интегральная и дефиринциальная", "Линейная и базовая", "базовая и дополнительная", "дополнительная и интегральная", "базовая и интегральная"},
            {"отношение разбиения на составляющие l.", "Отношение разбиения на детали l.", "Отношение разбиение на подзадачи l.", "Отношение разбиения на задачи."},
            {"Компьютерное средство обучения", "Компьютерно-обучающая система", "Компьютерно-педагогическая система", "Электронное средство обучения"},
            {"необходимость иметь компьютер и обладать навыками работы с ним", "сложность восприятия большого объема текстового материалы с экрана", "недостаточная интерактивность", "отсутствие непосредственного контроля", "возможность автоматического числа сгенерированных дополнительных заданий"},
            {"по временному фактору", "по степени «охвата» учащихся", "по реализации индивидуального подхода к учащимся", "по степени «механизации» педагогических операций", "по количеству обучаемой информации"}
    };
    private static int[] correct = new int[]{0, 3, 2, 4, 0, 4, 3, 0, 0, 4, 4};
    private List<QuestionTypeOne> list;

    private QuestionTypeOneList() {
        list = new ArrayList<>();
        for (int i = 0; i < questions.length; i++) {
            QuestionTypeOne question = new QuestionTypeOne();
            question.setQuestion(questions[i]);
            question.setVariants(new ArrayList<String>(convert(answers[i])));
            question.setAnswer(answers[i][correct[i]]);
            list.add(question);
        }
    }

    public static QuestionTypeOneList getInstance() {
        if (instance == null) {
            instance = new QuestionTypeOneList();
        } else {
            for (QuestionTypeOne question : instance.list) {
                question.setUserAnswer("");
            }
        }
        return instance;

    }

    public static List<String> convert(String[] array) {
        List<String> list = new ArrayList<>();
        for (String str : array) {
            list.add(str);
        }
        return list;
    }

    public List<QuestionTypeOne> getList() {
        return list;
    }
}
