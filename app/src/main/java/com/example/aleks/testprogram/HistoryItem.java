package com.example.aleks.testprogram;

import java.io.Serializable;

/**
 * Created by aleks on 31.03.2017.
 */

public class HistoryItem implements Serializable {
    private long start;
    private long duration;
    private int answer1;
    private int snawer2;

    public HistoryItem(long start, long duration, int answer1, int snawer2) {
        this.start = start;
        this.duration = duration;
        this.answer1 = answer1;
        this.snawer2 = snawer2;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getAnswer1() {
        return answer1;
    }

    public void setAnswer1(int answer1) {
        this.answer1 = answer1;
    }

    public int getSnawer2() {
        return snawer2;
    }

    public void setSnawer2(int snawer2) {
        this.snawer2 = snawer2;
    }
}
