package com.example.aleks.testprogram;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleks on 30.03.2017.
 */

public class QuestionTypeTwoList {
    private static QuestionTypeTwoList instance;
    private static String[] questions = new String[]{
            "Какие характеристики не составляю модель обучения:",
            "Для того, что бы повысить качество дистанционного обучения строится индивидуальная траектория. Что это траектория должна учитывать:",
            "Какие элементы входят в математическую модель:",
            "Какие принципы открытого образования являются основополагающими при формирования модели обучаемого:",
            "В виде каких элементов можно представить архитектуру процесса построение индивидуальной траектории обучения:",
            "Что не относится к организационным формам компьютерного средства обучения:",
            "Какие функции компьютер не может выполнять в обучении:",
            "Какие цели преследует компьютерное обучение:"
    };
    private static String[][] answers = new String[][]{
            {"Цели обучения", "Начальные знания обучаемого", "Его способности к обучению и восприятию учебного материала", "особенности подачи учебных материалов", "продолжительность обучения", "скорость забывания информации"},
            {"начальные знания обучаемого", "психофизические характеристики", "скорость запоминания", "скорость забывания", "личные качества обучаемого"},
            {"ИС- исходное состояние обучаемого", "М — мотивация и способность к обучению", "ТО — траектория обучения", "КЦ — конечная цель", "Adapt — процедура настройки траектории", "НИ — начальная информация", "ЗО — знания обучаемого", "СЗ — скорость забывания"},
            {"индивидуальность", "открытость", "гибкость обучения", "многоцельность", "массовость", "адаптированность"},
            {"база данных", "модуль обучения и тестирования знаний", "модуль формирования траектории обучения", "база знаний", "модуль обучения и тестирования обучаемого"},
            {"Демонстрация во весь экран", "Индивидуальная работа", "работа в малых группах", "работа в больших группах", "самообучение", "коллективная работа"},
            {"технико-педагогические", "дидактические", "стимулирующие", "координирующие", "контрольные"},
            {"развитие интеллектуальной сферы", "развитие мотивационной сферы", "развитие эмоциональной сферы", "развитие волевой сферы", "развитие познавательной сферы", "развитие структурной сферы", "развитие аналитической сферы"},
    };
    private static int[][] correct = new int[][]{
            {4, 5},
            {0, 1},
            {0, 1, 2, 3, 4},
            {0, 1, 2},
            {0, 1, 2},
            {0, 1, 2},
            {2, 3, 4},
            {0, 1, 2, 3}
    };
    private List<QuestionTypeTwo> list;

    private QuestionTypeTwoList() {
        list = new ArrayList<>();
        for (int i = 0; i < questions.length; i++) {
            QuestionTypeTwo question = new QuestionTypeTwo();
            question.setQuestion(questions[i]);
            question.setVariants((convert(answers[i])));
            question.setAnswer(convertAnswers(correct[i], i));
            question.setUserAnswer(new ArrayList<String>());
            list.add(question);
        }
    }

    public static QuestionTypeTwoList getInstance() {
        if (instance == null) {
            instance = new QuestionTypeTwoList();
        } else {
            for (QuestionTypeTwo question : instance.list) {
                question.setUserAnswer(new ArrayList<String>());
            }
        }
        return instance;

    }

    private static List<String> convertAnswers(int[] ints, int j) {
        List<String> stringList = new ArrayList<>();
        for (int i : ints) {
            stringList.add(answers[j][i]);
        }
        return stringList;
    }

    public static List<String> convert(String[] array) {
        List<String> list = new ArrayList<>();
        for (String str : array) {
            list.add(str);
        }
        return list;
    }

    public List<QuestionTypeTwo> getList() {
        return list;
    }
}
