package com.example.aleks.testprogram;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MaterialsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MaterialsFragment extends Fragment {

    private PDFView pdfView;

    public MaterialsFragment() {
        // Required empty public constructor
    }

    public static MaterialsFragment newInstance() {
        MaterialsFragment fragment = new MaterialsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_materials, container, false);
        pdfView = (PDFView) view.findViewById(R.id.pdfView);

        try {
            pdfView.fromStream(getContext().getAssets().open("lection.pdf"))
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    .load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }

}
