package com.example.aleks.testprogram;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {

    private List<HistoryItem> items;
    private ListView listView;
    private HistoryAdapter adapter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        listView = (ListView) view.findViewById(R.id.historyListView);
        getHistory();
        adapter = new HistoryAdapter(items);
        listView.setAdapter(adapter);
        return view;
    }

    private void getHistory() {
        File file = new File(getContext().getFilesDir(), "history");
        try {
            if (!file.exists()) {
                items = new ArrayList<>();
                return;
            }
            Log.d("mytag", file.getAbsolutePath());
            FileInputStream fin = null;
            ObjectInputStream ois = null;
            fin = new FileInputStream(file);
            try {
                ois = new ObjectInputStream(fin);
                items = (List<HistoryItem>) ois.readObject();
                ois.close();
            } catch (EOFException ex) {
                items = new ArrayList<>();
            }
            fin.close();
            Log.d("mytag", items.size() + "");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private class HistoryAdapter extends ArrayAdapter<HistoryItem> {

        HistoryAdapter(@NonNull List<HistoryItem> objects) {
            super(HistoryFragment.this.getContext(), R.layout.layout_history_item, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            HistoryItem question = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.layout_history_item, null);
            }
            TextView startTextView = (TextView) convertView.findViewById(R.id.historyItemStartTextView);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy");
            Date date = new Date(question.getStart());
            startTextView.setText("Дата тестирования:".concat(simpleDateFormat.format(date)));
            TextView durationTextView = (TextView) convertView.findViewById(R.id.historyItemDurationTextView);
            durationTextView.setText("Продолжительность:".concat(String.valueOf(TimeUnit.MILLISECONDS.toMinutes(question.getDuration()))));
            TextView result1TextView = (TextView) convertView.findViewById(R.id.historyItemResult1TextView);
            result1TextView.setText("Вопросы 1 сложности:".concat(question.getAnswer1() + "").concat("/11"));
            TextView result2TextView = (TextView) convertView.findViewById(R.id.historyItemResult2TextView);
            result2TextView.setText("Вопросы 2 сложности:".concat(question.getSnawer2() + "").concat("/8"));
            TextView resultTextView = (TextView) convertView.findViewById(R.id.historyItemResultTextView);
            resultTextView.setText("Результат:".concat(String.valueOf((int) (((double) (question.getAnswer1() + question.getSnawer2())) / 19 * 100))).concat("%"));
            return convertView;
        }
    }

}
