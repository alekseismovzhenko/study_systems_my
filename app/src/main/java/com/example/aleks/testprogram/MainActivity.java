package com.example.aleks.testprogram;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private TextInputLayout login;
    private TextInputLayout password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.loginButton);
        button.setOnClickListener(this);

        login = (TextInputLayout) findViewById(R.id.loginTextInputLayoutUsername);
        password = (TextInputLayout) findViewById(R.id.loginTextInputLayoutPassword);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton: {
                String str1 = login.getEditText().getText().toString();
                String str2 = login.getEditText().getText().toString();
                Intent intent = new Intent(this, TestActivity.class);
                if (str1.equals("student") && str2.equals("student")) {
                    intent.putExtra("trig", true);
                    startActivity(intent);
                } else if (str1.equals("teacher") && str2.equals("teacher")) {
                    intent.putExtra("trig", false);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Нет такого пользователя", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }
}
